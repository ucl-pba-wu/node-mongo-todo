const express = require('express');
const { default: mongoose } = require('mongoose');
const router = express.Router();
const Task = require('./../models/Task');

router.use(express.json());

//Create a new task
router.post('/', function (req, res) {
  //console.log(req.body.title, req.body.completed);
  Task.create(
    {
      title: req.body.title,
      completed: req.body.completed,
    },
    function (err, task) {
      if (err)
        return res
          .status(500)
          .send(
            'There was a problem adding the information to the database.\nDid you include all needed fields to the JSON request ?'
          );
      res.status(200).send(task);
    }
  );
});

//Room.find({}).sort({date: -1}).exec((err, docs) => { ... });
//return all tasks in the database
router.get('/all/:sortOrder', function (req, res) {
  const sortOrder = req.params.sortOrder
  Task.find({}).sort({creationDate: sortOrder}).exec(function (err, task) {
    if (err)
      return res
        .status(500)
        .send('There was a problem finding the tasks in the database');
    res.status(200).send(task);
  });
});

//find title in text search
router.get('/search/:searchQuery/:sortOrder', function(req, res) {
  console.log(req.params.searchQuery, req.params.sortOrder)
  const sortOrder = req.params.sortOrder
  const search = req.params.searchQuery;
  Task.find({ $text: { $search: search } }).sort({creationDate: sortOrder}).exec(function (err, task){
    if (err)
      return res
        .status(500)
        .send('There was a problem finding the tasks in the database');
    res.status(200).send(task);
  });
});


// GETS A SINGLE TASK FROM THE DATABASE
router.get('/:id', function (req, res) {
  const id = req.params.id;
  //console.log(id);
  Task.findById(id, function (err, task) {
    if (err)
      return res.status(500).send('There was a problem finding the task.');
    if (!task)
      return res.status(404).send('No task found with id ' + req.params.id);
    res.status(200).send(task);
    //console.log(task);
  });
});

//TODO: handle when err is NULL (no item with given id in db)
// DELETES A TASK FROM THE DATABASE
router.delete('/:id', function (req, res) {
  Task.findByIdAndRemove(req.params.id, function (err, task) {
    if (err)
      return res
        .status(500)
        .send(
          'There was a problem deleting the task with id: ' + req.params.id
        );
    res
      .status(200)
      .send(
        'task id ' + task.id + ' with the title ' + task.title + ' was deleted.'
      );
  });
});

router.put('/:id', function (req, res) {
  console.log(req.params.id, req.body);
  /* 	#swagger.description = 'Endpoint to update a specific task by id' */
  //console.log(mongoose.isValidObjectId(req.body.id))
  /*	#swagger.parameters['obj'] = {
            in: 'body',
            description: 'Task',
            required: true,
            schema: { $ref: "#/definitions/UpdateTask" }
    } */
  Task.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true, upsert: true},
    function (err, task) {
      console.log(err, task)
      if (err)
        return res
          .status(500)
          .send('There was a problem updating the task. ' + err.message);
      res.status(200).send(task);
    }
  );
});

module.exports = router;
