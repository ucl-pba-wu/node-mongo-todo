require('dotenv').config();
const mongoose = require('mongoose');

//const uri = process.env.ATLAS_CON_STRING
const uri = process.env.LOCAL_DB

mongoose.connect(uri, {useNewUrlParser: true})
.then(() => {
    console.log('db connected...');
})
.catch(err => console.log(err));

