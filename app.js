const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;
const db = require('./db');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger-output.json')

const TaskController = require('./controllers/TaskController');

app.use(cors());
app.use('/tasks', TaskController);
app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerFile))
app.listen(port, () => 
console.log(`todo app is listening on port http://localhost:${port}`))

// app.get('/', (req, res) => res.json({
//     "greeting": "Welcome to the todo app api",
//     "documentation_url": "/" 
// }))